# build container
docker build -t py/tf1 .

# run script
docker run -it --rm \
  -v "$PWD":/usr/src/app \
  py/scrape python3 ./cnns/cnn_mnist.py
